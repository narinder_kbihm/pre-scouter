// set variables for environment
var express = require('express');
var app = express();
var path = require('path');
var routes = require('./routes.js');
var http = require('http');
var fs = require("fs");
var path = require('path');
app.use(express.static('public'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs'); 		
// instruct express to server up static assets
app.use(express.static('public'));
// set routes
app.get('/', function(req, res) {
  res.render('index');
});
app.get('/search/', function(req, res, next)
{// Get content from file
 var contents = fs.readFileSync("response.json");
// Define to JSON type
 var jsonContent = JSON.parse(contents);
 res.json(jsonContent);
});
// Set server port
app.listen(4000);
console.log('server is running at http://localhost:4000/');
